/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/**
 *
 * @author batoi
 */
public class ConexionBD {

    public XmlRpcClient client;
    public final String DB = "batoilogic";
    public final String USER = "grup4@cipfpbatoi.es";
    public final String PASS = "1234";
    public int uid = 0;

    public ConexionBD(){
        
    }
    
    public XmlRpcClient getInstance() throws MalformedURLException, XmlRpcException {
        if (client == null) {
            client = new XmlRpcClient();
            XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
            config.setEnabledForExtensions(true);
            config.setServerURL(new URL("http", "grup4.cipfpbatoi.es", 80, "/xmlrpc/2/object"));
            client.setConfig(config);
            config = new XmlRpcClientConfigImpl();
            config.setEnabledForExtensions(true);
            config.setServerURL(new URL("http", "grup4.cipfpbatoi.es", 80, "/xmlrpc/2/common"));
            uid = (int)client.execute(config, "authenticate", asList(DB, USER, PASS, emptyMap()));
            
        }
        return client;
    }

}
