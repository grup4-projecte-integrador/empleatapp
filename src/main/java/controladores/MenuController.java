/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import conexion.ConexionBD;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

/**
 *
 * @author batoi
 */
public class MenuController implements Initializable {

    private ConexionBD base = new ConexionBD();
    private XmlRpcClient conexion;
    
    @FXML
    public AnchorPane menuCamiones;
    @FXML
    public AnchorPane menuPedidos;
    @FXML
    public AnchorPane menuStock;
    @FXML
    public AnchorPane menuInicio;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {

            conexion = base.getInstance();
            menuInicio.toFront();

        } catch (MalformedURLException ex) {

            ex.printStackTrace();

        } catch (XmlRpcException ex) {

            ex.printStackTrace();

        }

    }


    public void abrirMenuCamiones() {
        menuCamiones.toFront();
    }
    
    public void abrirMenuInicio() {
        menuInicio.toFront();
    }
    
    public void abrirMenuPedidos() {
        menuPedidos.toFront();
    }
    
    public void abrirMenuStock() {
        menuStock.toFront();
    }
    
    public void salirApp() {
        ((Stage) menuStock.getScene().getWindow()).close();
        System.exit(0);
    }

}
